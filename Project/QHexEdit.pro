HEADERS = \
    MainWindow.h \
    QHexEdit/ArrayCommand.h \
    QHexEdit/QHexEdit.h \
    QHexEdit/QHexEditPrivate.h \
    QHexEdit/XByteArray.h

SOURCES = \
    main.cpp \
    MainWindow.cpp \
    QHexEdit/ArrayCommand.cpp \
    QHexEdit/QHexEdit.cpp \
    QHexEdit/QHexEditPrivate.cpp \
    QHexEdit/XByteArray.cpp

RESOURCES = \
    QHexEdit.qrc
